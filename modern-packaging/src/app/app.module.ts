import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes,RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AirBubbleComponent } from './home/air-bubble/air-bubble.component';
import { PackagingTapeComponent } from './home/packaging-tape/packaging-tape.component';
import { IndrustialTapeComponent } from './home/indrustial-tape/indrustial-tape.component';
import { FilamentTapeComponent } from './home/filament-tape/filament-tape.component';
import { IndrustrialPackagingMaterialComponent } from './home/indrustrial-packaging-material/indrustrial-packaging-material.component';
import { FooterComponent } from './footer/footer.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactComponent } from './contact/contact.component';
import { ProductsComponent } from './products/products.component';
import { AirRollComponent } from './products/air-roll/air-roll.component';
import { AirPouchComponent } from './products/air-pouch/air-pouch.component';
import { AirSheetComponent } from './products/air-sheet/air-sheet.component';
import { EpeRollComponent } from './products/epe-roll/epe-roll.component';
import { PrintedSelfComponent } from './products/printed-self/printed-self.component';

import { BrownTapeComponent } from './products/brown-tape/brown-tape.component';
import { CelloTapeComponent } from './products/cello-tape/cello-tape.component';
import { DoubleSidedTapeComponent } from './products/double-sided-tape/double-sided-tape.component';
import { DoubleSidedTissueComponent } from './products/double-sided-tissue/double-sided-tissue.component';
import { DoubleSidedFoamComponent } from './products/double-sided-foam/double-sided-foam.component';
import { FloorMarketingComponent } from './products/floor-marketing/floor-marketing.component';
import { EvaTapeComponent } from './products/eva-tape/eva-tape.component';
import { SurfaceTapeComponent } from './products/surface-tape/surface-tape.component';
import { MonoTapeComponent } from './products/mono-tape/mono-tape.component';
import { CrossTapeComponent } from './products/cross-tape/cross-tape.component';
import { StretchWrapComponent } from './products/stretch-wrap/stretch-wrap.component';
import { StrappingRollComponent } from './products/strapping-roll/strapping-roll.component';
import { SubMenuComponent } from './sub-menu/sub-menu.component';
import { VciPaperComponent } from './products/vci-paper/vci-paper.component';
import { VciFilmComponent } from './products/vci-film/vci-film.component';


const appRoutes: Routes =[
  { path :'',component:HomeComponent,data:{title: 'Modern Packaging'}},
  {path:'contact',component:ContactComponent,data: {title: 'Contact us'}},

  {path:'about',component:AboutUsComponent,data:{title: 'About us | Modernpackaging'}},
  {path:'product',component:ProductsComponent,data:{title: 'All products | Modernpackaging'}},
  {path:'product/air-bubble-roll-manufacturer',component:AirRollComponent,data: {title: 'Air Bubble Roll Manufacturer'}},
  {path:'product/air-bubble-pouch-manufacturer',component:AirPouchComponent,data: {title: 'Air Bubble Pouch Manufacture'}},
  {path:'product/air-bubble-sheet-manufacturer',component:AirSheetComponent,data: {title: 'Air Bubble Sheet Manufacturer'}},
  {path:'product/epe-foam-roll-manufacturer',component:EpeRollComponent,data: {title: ' EPE Foam Roll Manufacturer '}},
  {path:'product/custom-printed-self-adhesive-tape',component:PrintedSelfComponent,data: {title: 'Custom Printed Self adhesive tap'}},
  {path:'product/brown-self-adhesive-tape-manufacturer',component:BrownTapeComponent,data: {title: ' Brown Adhesive Tape Manufacturer '}},
  {path:'product/cello-tape-manufacturer',component:CelloTapeComponent,data: {title: 'Cello Tape Manufacturer'}},
  {path:'product/double-sided-tape-manufacturer',component:DoubleSidedTapeComponent,data: {title: 'Double Sided Tape Manufacture'}},
  {path:'product/double-sided-tissue-tape-manufacturer',component:DoubleSidedTissueComponent,data: {title: 'Double Sided Tissue Tape Manufacturer'}},
  {path:'product/double-sided-foam-tape-manufacturer',component:DoubleSidedFoamComponent,data: {title: 'Double Sided Foam Tape Manufacturer'}},
  {path:'product/floor-marking-tape',component:FloorMarketingComponent,data: {title: 'Floor Marking Tape Manufacturer'}},
  {path:'product/eva-foam-tape-manufacturer',component:EvaTapeComponent,data: {title: 'Eva Foam Tape Manufacturer'}},
  {path:'product/surface-protection-tape-manufacturer',component:SurfaceTapeComponent,data: {title: ' Surface Protection Tape Manufacturer '}},
  {path:'product/mono-filament-tape',component:MonoTapeComponent,data: {title: 'Mono Filament Tape Manufacturer'}},
  {path:'product/cross-filament-tape',component:CrossTapeComponent,data: {title: 'Cross Filament Tape Manufacturer'}},
  {path:'product/stretch-wrap-film-manufacturer',component:StretchWrapComponent,data: {title: 'Stretch Wrap Film Manufacturer'}},
  {path:'product/strapping-roll',component:StrappingRollComponent,data: {title: 'Strapping Roll Manufacturer'}},
  {path:'product/vci-paper',component: VciPaperComponent,data: {title: 'Volatile Corrosion Inhibitor- VCI Paper Manufacturer'}},
  {path:'product/vci-film-bags',component: VciFilmComponent,data: {title: 'Volatile Corrosion Inhibitor Film & VCI Bags Manufacturer'}}



];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AirBubbleComponent,
    PackagingTapeComponent,
    IndrustialTapeComponent,
    FilamentTapeComponent,
    IndrustrialPackagingMaterialComponent,
    FooterComponent,
    AboutUsComponent,
    ContactComponent,
    ProductsComponent,
    AirRollComponent,
    AirPouchComponent,
    AirSheetComponent,
    EpeRollComponent,
    PrintedSelfComponent,

    BrownTapeComponent,
    CelloTapeComponent,
    DoubleSidedTapeComponent,
    DoubleSidedTissueComponent,
    DoubleSidedFoamComponent,
    FloorMarketingComponent,
    EvaTapeComponent,
    SurfaceTapeComponent,
    MonoTapeComponent,
    CrossTapeComponent,
    StretchWrapComponent,
    StrappingRollComponent,
    SubMenuComponent,
    VciPaperComponent,
    VciFilmComponent,

  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'modern-packaging' }),
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {


}
