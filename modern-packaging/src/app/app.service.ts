import { Injectable } from '@angular/core';
import { Contact } from './contact';
import { map } from 'rxjs/operators';
import { HttpClient,HttpErrorResponse ,HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AppService {
  serverUrl = "hhttps://4waytechnologies.com/modernmail/mail.php";
  httpOptions = {
    headers : new HttpHeaders({'Content-Type' : 'multipart/form-data'})
  };
  constructor( private http: HttpClient,private router :Router) { }

  contactform(formdata :Contact ){
    console.log(formdata.name)
    if(formdata.name == undefined){
      alert("Please Enter Your Name");
      return;
     }
      else if(formdata.phone == undefined){
        alert("Please Enter Your Phone Number");

      }else if(formdata.email == undefined){
        alert("Please Enter Your EmailID");
        return;
      }else if(formdata.msg == undefined){
        alert("Please Enter Description");
        return;
      }
      else{
          var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
          var phone = formdata.phone;
          var phoneNum = phone.replace(/[^\d]/g, '');
          if(reg.test(formdata.email) == false){
            alert("Please enter valid Email");
          }
          else if(phoneNum.length <= 6 || phoneNum.length >=11){
            alert("please Enter valid Phone Number")
          }
          else{
            alert("Your Form Successfully Submitted !!");
            console.log(formdata);
           // this.http.post(this.serverUrl,formdata).pipe(map((response: any) => response.json()));
           this.router.navigate(['/']);
            return this.http.post<Contact>(this.serverUrl,formdata,this.httpOptions);
          }


    }

  }





}
