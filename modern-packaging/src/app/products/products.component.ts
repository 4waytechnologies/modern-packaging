import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {


  products = [
        {
          id:0,
          name: 'Air Bubble Roll',
          img: '../assets/images/air-bubble-roll.png'
        },
        {
          id:1,
          name: 'Air Bubble Pouch',
          img:'../assets/images/air bubble pouch.png'
        },
        {
          id:2,
          name : 'Air Bubble Sheet',
          img : '../assets/images/air bubble sheet.png'
        },
        {
          id:3,
          name:' EPE ROll ',
          img:'../assets/images/EPE FOAM.png'
        }

  ];
  constructor(private route: ActivatedRoute ) { }

  ngOnInit() {

      var id = this.route.snapshot.params['id']
      sessionStorage.setItem("id",id);


  }

}
