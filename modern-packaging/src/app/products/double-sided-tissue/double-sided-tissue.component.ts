import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import { Routes,RouterModule,Router } from '@angular/router';
import { Contact } from '../../contact';

@Component({
  selector: 'app-double-sided-tissue',
  templateUrl: './double-sided-tissue.component.html',
  styleUrls: ['./double-sided-tissue.component.css']
})
export class DoubleSidedTissueComponent implements OnInit {

  model = new Contact();
  submittedForm = false;
  error = {};

    constructor(
      private router :Router,
      private appservice : AppService
    ) {}

    ngOnInit() {
    }
  onsubmit(){
    this.submittedForm = true;
    return this.appservice.contactform(this.model).subscribe(
      data => this.model = data,

      error => this.error = error
    );
    this.gotohome();
  }
  gotohome(){
    this.router.navigate(['https://4waytechnologies.com/mordernmail.php']);
  }
}
