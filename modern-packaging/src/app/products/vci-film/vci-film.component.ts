import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import { Routes,RouterModule,Router } from '@angular/router';
import { HttpClient,HttpErrorResponse ,HttpHeaders } from '@angular/common/http';
import { Contact } from '../../contact';
@Component({
  selector: 'app-vci-film',
  templateUrl: './vci-film.component.html',
  styleUrls: ['./vci-film.component.css']
})
export class VciFilmComponent implements OnInit {
  model = new Contact();
  submittedForm = false;
  error = {};

    constructor(
      private router :Router,
      private appservice : AppService,
      private http: HttpClient,
    ) {}

    ngOnInit() {
    }
  onsubmit(){
    
    this.submittedForm = true;
    return this.appservice.contactform(this.model).subscribe(
      data => this.model = data,

      error => this.error = error
    );
    this.gotohome();
    
  }
  gotohome(){
    this.router.navigate(['/']);
  }

}
