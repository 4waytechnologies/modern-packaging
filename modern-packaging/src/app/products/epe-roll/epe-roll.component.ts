import { Component, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import { Routes,RouterModule,Router } from '@angular/router';
import { Contact } from '../../contact';

@Component({
  selector: 'app-epe-roll',
  templateUrl: './epe-roll.component.html',
  styleUrls: ['./epe-roll.component.css']
})
export class EpeRollComponent implements OnInit {
  model = new Contact();
  submittedForm = false;
  error = {};

    constructor(
      private router :Router,
      private appservice : AppService
    ) {}

    ngOnInit() {
    }
  onsubmit(){
    this.submittedForm = true;
    return this.appservice.contactform(this.model).subscribe(
      data => this.model = data,

      error => this.error = error
    );
    this.gotohome();
  }
  gotohome(){
    this.router.navigate(['https://4waytechnologies.com/mordernmail.php']);
  }
}
